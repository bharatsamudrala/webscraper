from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from utilities import get_logger, zipDownloadDirectory


def main():
    logger = get_logger(__name__)
    logger.info('Crawl intiation')

    settings = get_project_settings()
    process = CrawlerProcess(settings)
    # process.crawl('robotsspider')
    process.crawl('mhraspider')
    zipDownloadDirectory('mhra')
    # process.crawl('suburl')
    # process.crawl('dynamicContent')
    process.start()

if __name__ == '__main__':
    main()