import psycopg2
from psycopg2.extensions import adapt
import os
import glob
import datetime
import logging
from boto3 import Session
import shutil


def get_logger(filename):
    logging.basicConfig(filename=filename, level=logging.DEBUG,
                        format='%(asctime)s \t[%(name)-s] \t%(filename)-s \t%(lineno)-d'
                               ' \t%(levelname)-s: \t%(message)s \r',
                        datefmt='%Y-%m-%dT%T%Z')
    logger = logging.getLogger(filename)

    return logger

logger = get_logger(__name__)

def getStartUrls():
    fetchUrls = []
    try:
        cur = dbConnect().cursor()
        cur.execute("SELECT distinct program_url FROM keepers_view where program_url notnull;")
        urls = cur.fetchall()
        for url in urls:
            fetchUrls.append(url[0])
        cur.close()
        dbConnect().close()
        return fetchUrls
    except psycopg2.OperationalError as e:
        logger.info("Could not connect to server. Check the database connections")
        logger.info(e)


def formatting():
    robots_urls = []
    urls = getStartUrls()
    # for url in urls:
    #    robots_urls.append(url + '/robots.txt')
    return urls


def dbConnect():
    try:
        conn = psycopg2.connect(host='18.217.130.132', user='postgres', password='pos123', dbname='medmeme')
        return conn
    except Exception as e:
        return e


def get_s3_client():
    session = Session(
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY')
    )

    return session.client('s3')


def s3_exists(client, bucket, key):
    resp = client.list_objects(
        Bucket=bucket,
        Prefix=key)
    expected_object = resp.get('Contents', [])

    return bool(expected_object)


def createfile(filename):
    f = open(os.getcwd() + filename, 'wb')
    return f

def formatItemtodb(item):
    data = adapt(item)
    return data

def getAllowedDomains():
    allowed_domains = []
    try:
        cur = dbConnect().cursor()
        cur.execute("SELECT distinct program_url FROM keepers_view where program_url notnull;")
        urls = cur.fetchall()
        for url in urls:
            if 'http://' in url[0]:
                temp = url[0].split('//')[1]
                domain = temp.split('/')[0]
            elif 'https://' in url[0]:
                temp = url[0].split('//')[1]
                domain = temp.split('/')[0]
            allowed_domains.append(domain)
        cur.close()
        dbConnect().close()
        return allowed_domains
    except psycopg2.OperationalError as e:
        logger.info("Could not connect to server. Check the database connections")
        logger.info(e)


def get_temp_download_directory(dir_name):
    path = os.path.join(os.getcwd(), 'downloads', dir_name)
    if os.path.exists(path):
        return os.path.join(os.getcwd(), 'downloads', dir_name)
    else:
        os.mkdir(os.path.join(os.getcwd(), 'downloads'))
        os.mkdir(os.path.join(os.getcwd(), 'downloads', dir_name))
        return os.path.join(os.getcwd(), 'downloads', dir_name)



def zipDownloadDirectory(output_filename):
    zippedFile = shutil.make_archive(output_filename, 'zip', get_temp_download_directory(output_filename))
    s3_root = os.path.join('data', datetime.datetime.now().strftime("%Y/%m/%d"), 'unstructured', 'raw', output_filename)
    s3_file_key = os.path.join(s3_root, zippedFile)
    uploadToS3(zippedFile, 'mmcapturepoctemp', s3_file_key)


def uploadToS3(file_location, bucket, s3_file_key):
    s3_client = get_s3_client()
    if not s3_exists(s3_client, bucket, s3_file_key):
        s3_client.upload_file(file_location, bucket, s3_file_key)
        logger.info('Saved {} to download directory'.format(file_location))
    else:
        logger.info('File {} already exists in S3' .format(file_location))
    return
