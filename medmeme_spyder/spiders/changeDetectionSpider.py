from scrapy.spiders import CrawlSpider, Rule, Request
from scrapy.linkextractors import LinkExtractor
from utilities import getAllowedDomains, getStartUrls, dbConnect
import psycopg2


class LinkExtract(CrawlSpider):
    name = 'changedetectspider'

    def __init__(self):
        super(LinkExtract, self).__init__()
        self.start_urls = getStartUrls(self)
        self.allowed_domains = getAllowedDomains(self)

    rules = (Rule(LinkExtractor(), callback='parse_extract', follow=True),)

    def start_requests(self):
        print('from start request')
        for url in self.start_urls:
            yield Request(url, method='HEAD', dont_filter=True)

    def parse(self, response):
        domainname = response.url.split('/')[2]
        try:
            dbConnection = dbConnect()
            cur = dbConnection.cursor()
            lm = ''
            cl = ''
            if 'Last-Modified' in response.headers:
                lm = response.headers['Last-Modified'].decode("utf-8")
            elif 'Content-Length' in response.headers:
                cl = response.headers['Content-Length'].decode("utf-8")
            else:
                raise KeyError
            cur.execute("SELECT seedurlid from seedurl where domainname='%s';" % domainname)
            seedurlid = cur.fetchall()
            for id in seedurlid:
                if lm != '' :
                    cur.execute(
                        "UPDATE seedurl set lastmodified = '%s' where seedurlid = '%s';" % (
                            lm, id[0]))
                elif cl != '':
                    cur.execute(
                        "UPDATE seedurl set contentlength = '%s' where seedurlid = '%s';" % (
                            cl, id[0]))
                elif cl != '' and lm != '':
                    cur.execute("UPDATE seedurl set contentlength = '%s', lastmodified = '%s' where seedurlid = '%s';" %
                                (cl, lm, id[0]))
                else:
                    raise psycopg2.Error
            dbConnection.commit()
        except psycopg2.Error as e:
            dbConnection.rollback()
            self.logger(e)
        except KeyError as e:
            self.logger(e)
            pass
        finally:
            cur.close()
            dbConnection.close()
