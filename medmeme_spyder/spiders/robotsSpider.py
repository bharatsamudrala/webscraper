# -*- coding: utf-8 -*-


import scrapy
from utilities import createfile, s3_exists, get_s3_client, get_logger, get_temp_download_directory, dbConnect
from medmeme_spyder.items import MedmemeSpyderItem
from scrapy.loader import ItemLoader
import datetime
import socket
import os
import psycopg2

logger = get_logger('robotsSpiderLog')

class RobotsSpider(scrapy.Spider):
    name = 'robotsspider'

    def __init__(self):
        super(RobotsSpider, self).__init__()
        with open('/Users/bharatsamudrala/PycharmProjects/medmeme-scrapy-crawler/abstract_urls', 'r') as surls:
            urls = surls.readlines()
            for url in urls:
                self.start_urls.append(url.rstrip())

        # self.start_urls = formatting()
        # self.allowed_domains = getAllowedDomains()
            self.s3_client = get_s3_client()
            self.bucket = 'mmcapturepoctemp'
            self.s3_root = os.path.join(
                            'data',
                            datetime.datetime.now().strftime('%Y/%m/%d'),
                            'unstructured',
                            'raw'
                            )
            self.upload_to_s3 = False

    def parse(self, response):
        if response.status == 200:
            filename = response.url.replace('/', '~')
            s3_file_key = os.path.join(self.s3_root, filename)
            location = get_temp_download_directory()
            file_location = os.path.join(location, filename)

            if not s3_exists(self.s3_client, self.bucket, s3_file_key):
                self.upload_to_s3 = True


            f = createfile(filename)
            f.write(response.body)

            if self.upload_to_s3:
                self.s3_client.upload_file(
                    file_location,
                    self.bucket,
                    s3_file_key)
                logger.info('Moved {} to S3'.format(
                    filename))
                # os.remove(filename)
                # logger.info('Removed {} from local' .format(filename))

            itemR = ItemLoader(item=MedmemeSpyderItem(), response=response)

            itemR.add_value('url', response.request.url)
            itemR.add_value('statuscode', response.status)
            itemR.add_value('project', self.settings.get('BOT_NAME'))
            itemR.add_value('spider', self.name)
            itemR.add_value('server', socket.gethostname())
            itemR.add_value('date', datetime.datetime.now().isoformat())
            itemR.add_value('schema', 'http://json-schema.org/draft-06/schema#')
            itemR.add_value('title', 'Website Metadata')
            itemR.add_value('description', 'Contains metadata of web-scraping')
            itemR.add_value('domain_name', response.url.split('/')[2])
            itemR.add_value('data_download_location', s3_file_key)
            return itemR.load_item()
        else:
            conn = dbConnect()
            try:
                cur = conn.cursor()
                cur.execute("select event_id from keepers_view where program_url = '%s';" % response.request.url)
                event_ids = cur.fetchall()
                for event_id in event_ids:
                    cur.execute("INSERT into urlattributes(event_id, processmeta, status_code) values('%s','', '%s')"
                                % (event_id[0], response.status))
            except psycopg2.Error as e:
                conn.rollback()
                logger.info('Upload failed on postgres pipeline')
                logger.info(e)
            else:
                conn.commit()
            finally:
                conn.close()