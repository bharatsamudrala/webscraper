# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 11:09:21 2018
@author: Abhinay Reddy
"""
import scrapy
from scrapy.http import Request
import os
from utilities import get_logger, get_temp_download_directory

logger = get_logger('mhraSpiderLog')


class mhraSpider(scrapy.Spider):
    name = "mhraspider"
    start_urls = ["http://www.mhra.gov.uk/spc-pil/index.htm?indexChar=A#retainDisplay"]
    allowed_domains = ["www.mhra.gov.uk"]

    domain_attach = "http://www.mhra.gov.uk/spc-pil/index.htm"

    def __init__(self):
        super(mhraSpider, self).__init__()
        self.start_urls = ["http://www.mhra.gov.uk/spc-pil/index.htm?indexChar=A#retainDisplay"]
        self.allowed_domains = ["www.mhra.gov.uk"]

        domain_attach = "http://www.mhra.gov.uk/spc-pil/index.htm"

    def parse(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "?indexChar=" in href:
                href = self.domain_attach + href
                # print("-----level_1_big_alphabets--------", href, "_________________")
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_small_alphabets
                )

    def parse_small_alphabets(self, response):
       for href in response.css('a::attr(href)').extract():
            # href = self.domain_attach.join(href)
            # print("----------level2-----------", href)
            if "?secLevelIndexChar=" in href:
                # print("-----level_2_small_alphabets--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_subs
                )

    def parse_subs(self, response):
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "?subsName=" in href:
                # print("-----subs--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_products
                )

    def parse_products(self, response):
        current_address_url = response.request.url
        for href in response.css('a::attr(href)').extract():
            # print("----------href-----------",href)
            if "?prodName=" in href and href not in current_address_url:
                # print("-----products--------", href)
                yield Request(
                    url=response.urljoin(href),
                    callback=self.parse_article
                )

    def parse_article(self, response):
        for href in response.css('a[href$=".pdf"]::attr(href)').extract():
            # print("***********PDF_________________", href)
            if "/home/groups/spcpil/documents/spcpil/" in href:
                yield Request(
                    url=response.urljoin(href),
                    callback=self.save_pdf
                )

    def save_pdf(self, response):
        filename = response.url.replace('/', '~')
        location = get_temp_download_directory('mhra')
        file_location = os.path.join(location, filename)
        if response.status == 200:
            with open(file_location, 'wb') as f:
                f.write(response.body)
                logger.info('Saved {} to download directory'.format(filename))
        else:
            logger.info('Not a 200  status - filename {}'.format(filename))
            logger.info('** ** ** STATUS CODE {} and url {}'.format(response.status, response.request.url))