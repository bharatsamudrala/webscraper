# -*- coding: utf-8 -*-
import scrapy
from utilities import formatting, createfile, getAllowedDomains, s3_exists, get_s3_client
from scrapy_splash import SplashRequest
from scrapy.linkextractors import LinkExtractor
import os
import datetime

script = """function main(splash)
    assert(splash:go(splash.args.url))
    assert(splash:wait(0.5))

    local imgs = splash:select_all('a')
    local srcs = {}
  	local html = {}

    for _, img in ipairs(imgs) do
    	if img.node.attributes.onclick then
      	srcs[#srcs+1] = img.node.attributes
      	end
    end
    for _, cl in ipairs(srcs) do
    	formt = cl["class"]
    	--tosring(format)
    	if formt then
      	formt = string:gsub(" ",".")
      else
     end
    end
    return treat.as_array(srcs)
end
                
"""

scrpit_ema_type_usecase = """function main(splash)
    assert(splash:go(splash.args.url))
    assert(splash:wait(0.5))

    local imgs = splash:select_all('checkbox')
    local srcs = {}
  	local html = {}

    for _, img in ipairs(imgs) do
    	if img.node.attributes.onclick then
      	srcs[#srcs+1] = img.node.attributes
      	end
    end
    for _, cl in ipairs(srcs) do
    	formt = cl["class"]
    	--tosring(format)
    	if formt then
      	formt = string:gsub(" ",".")
      else
     end
    end
    return treat.as_array(srcs)
end

"""
class DynamiccontentSpider(scrapy.Spider):
    name = 'dynamicContent'

    def __init__(self):
        super(DynamiccontentSpider, self).__init__()
        self.start_urls = formatting(self)
        self.allowed_domains = getAllowedDomains(self)
        self.s3_client = get_s3_client()
        self.bucket = 'mmcapturepoctemp'
        self.s3_root = os.path.join(
                        'data',
                        datetime.datetime.now().strftime('%Y/%m/%d'),
                        'structured',
                        'raw'
                        )

    def parse(self, response):
        le = LinkExtractor()
        for link in le.extract_links(response):
            yield SplashRequest(
                link.url,
                self.parse_link,
                endpoint='render.html',
                args={
                    'har': 1,
                    'html': 1,
                }
            )

    def parse_link(self, response):
        filename = response.url.replace('/', '~')
        s3_file_key = os.path.join(self.s3_root, filename)

        if not s3_exists(self.s3_client, self.bucket, s3_file_key):
            self.upload_to_s3 = True

        if response.status == 200:
            f = createfile(self, filename)
            f.write(response.body)

        if self.upload_to_s3:
            self.s3_client.upload_file(
                filename,
                self.bucket,
                s3_file_key)
            os.remove(filename)

            self.logger.debug('Moved {} to S3'.format(
                filename))