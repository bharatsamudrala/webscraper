# -*- coding: utf-8 -*-

# CrawlSpider to extract all the available links in a given website recursively


from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from utilities import getStartUrls, createfile, getAllowedDomains, s3_exists, get_s3_client, dbConnect
import logging
import psycopg2
from medmeme_spyder.items import MedmemeSpyderItem
from scrapy.loader import ItemLoader
import socket
import datetime
import os

logger = logging.getLogger(__name__)
class SuburlSpider(CrawlSpider):
    logger.info("Hello from suburl spider")
    name = 'suburl'

    # Get starturls from dB
    def __init__(self):
        super(SuburlSpider, self).__init__()
        self.start_urls = getStartUrls(self)
        self.allowed_domains = getAllowedDomains(self)
        self.s3_client = get_s3_client()
        self.bucket = 'mmcapturepoctemp'
        self.s3_root = os.path.join(
            'data',
            datetime.datetime.now().strftime('%Y/%m/%d'),
            'structured',
            'raw'
        )

    # Rule to define the crawling behaviour with link extractor
    rules = (Rule(LinkExtractor(allow=()), callback='parse_extract', follow=True),)

    # Capture the extracted data and save to local directory
    def parse_extract(self, response):
        domainname = response.url.split('/')[2]
        active = True
        if response.status > 299:
            active = False
        try:
            dbconnection = dbConnect()
            cur = dbconnection.cursor()
            cur.execute("SELECT seedurlid from seedurl where domainname='%s';" % (domainname))
            seedurlid = cur.fetchall()
            for id in seedurlid:
                cur.execute("INSERT into pageurl(seedurlid, starturl, active) VALUES ('%s', '%s', '%s');" % (id[0], response.request.url, active))
                dbconnection.commit()
        except psycopg2.Error as e:
            dbconnection.rollback()
            logger.info(e)
        finally:
            cur.close()
            dbconnection.close()

        filename = response.url.replace('/', '~')
        s3_file_key = os.path.join(self.s3_root, filename)

        if not s3_exists(self.s3_client, self.bucket, s3_file_key):
            self.upload_to_s3 = True

        if response.status == 200:
            f = createfile(self, filename)
            f.write(response.body)

        if self.upload_to_s3:
            self.s3_client.upload_file(
                filename,
                self.bucket,
                s3_file_key)
            os.remove(filename)

            self.logger.debug('Moved {} to S3'.format(
                filename))

        # Create a loader using the response
        l = ItemLoader(item=MedmemeSpyderItem(), response=response)

        # Load fields using xpath
        # l.add_xpath('sitemap', '//*')

        # Housekeeping fields
        l = ItemLoader(item=MedmemeSpyderItem(), response=response)

        # Housekeeping fields
        l.add_value('url', response.url)
        l.add_value('project', self.settings.get('BOT_NAME'))
        l.add_value('spider', self.name)
        l.add_value('server', socket.gethostname())
        l.add_value('date', datetime.datetime.now().isoformat())
        l.add_value('schema', 'http://json-schema.org/draft-06/schema#')
        l.add_value('title', 'Website Metadata')
        l.add_value('description', 'Contains metadata of web-scraping')
        l.add_value('domain_name', response.url.split('/')[2])
        l.add_value('data_download_location', self.s3_root + filename)
        return l.load_item()