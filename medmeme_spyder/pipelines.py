# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

hostname = '18.217.130.132'
username = 'postgres'
password = 'pos'
database = 'medmeme'

import psycopg2
from utilities import dbConnect, formatItemtodb, get_logger

logger = get_logger(__name__)

class MedmemeSpyderPipeline(object):
    def process_item(self, item, spider):
        return item

class PostgresqlPipeline(object):
    """Pipeline that writes scrapy items to postgresql"""

    def process_item(self, item, spider):
        try:
            tostring = str(item)
            conn = dbConnect()
            cur = conn.cursor()
            cur.execute("select event_id from keepers_view where program_url = '%s';" % (item['url'][0]))
            event_ids = cur.fetchall()
            for event_id in event_ids:
                cur.execute("INSERT into urlattributes(event_id, processmeta, status_code) values('%s',%s, '%s')"
                            % (event_id[0], formatItemtodb(tostring), item['statuscode']))
        except psycopg2.Error as e:
            conn.rollback()
            logger.info('Upload failed on postgres pipeline')
            logger.info(e)
        else:
            conn.commit()
        finally:
            cur.close()
            conn.close()
