# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import logging
from scrapy import signals
from scrapy.exceptions import IgnoreRequest
import psycopg2
from utilities import dbConnect, formatItemtodb
from scrapy.exceptions import NotConfigured
from scrapy.utils.response import response_status_message
from scrapy.utils.python import global_object_name
from twisted.internet.error import TimeoutError, DNSLookupError, \
        ConnectionRefusedError, ConnectionDone, ConnectError, \
        ConnectionLost, TCPTimedOutError
from twisted.internet import defer
from twisted.web.client import ResponseFailed
from scrapy.core.downloader.handlers.http11 import TunnelError

logger = logging.getLogger(__name__)

hostname = 'localhost'
username = 'postgres'
password = 'sbbharaT91'
database = 'meme'


class HttpError(IgnoreRequest):
    """A non-200 response is filtered"""

    def __init__(self, response, *args, **kwargs):
        self.response = response
        super(HttpError, self).__init__(*args, **kwargs)


class MedmemeSpyderSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class MedmemeSpyderDownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class MedmemeSpyderHttpErrorMiddleware(object):
    def process_spider_input(self, response, spider):
        dbConnection = dbConnect()
        cur = dbConnection.cursor()
        if 300 < response.status < 500:
            if "robots" in response.url:
                try:
                    cur.execute("select event_id from keepers_view where program_url = '%s';" % response.url)
                    event_ids = cur.fetchall()
                    for event_id in event_ids:
                        cur.execute("UPDATE urlattributes SET robots_status_code='%s' where event_id=%s;"
                                %(formatItemtodb(response.status), event_id[0]))
                except psycopg2.Error as e:
                    dbConnection.rollback()
                    logger.info(e)
                else:
                    dbConnection.commit()
                finally:
                    cur.close()
                    dbConnection.close()
        elif response.status == 200:
            if "robots" in response.url:
                try:
                    cur.execute("select event_id from keepers_view where program_url = '%s';" % response.url)
                    event_ids = cur.fetchall()
                    for event_id in event_ids:
                        cur.execute("UPDATE urlattributes SET robots_status_code='%s' where event_id=%s;"
                                % (formatItemtodb(response.status), event_id[0]))
                except psycopg2.Error as e:
                    dbConnection.rollback()
                    logger.info(e)
                else:
                    dbConnection.commit()
                finally:
                    cur.close()
                    dbConnection.close()


    def process_spider_exception(self, response, exception, spider):
        if isinstance(exception, HttpError):
            spider.crawler.stats.inc_value('invalid_robotsorsitemap_url')
            spider.crawler.stats.inc_value('invalid_robotsorsitemap_url/%s' %response.url)
            logger.info("Ignoring response %(response)r: HTTP status code is not handled or not allowed",
                        {'response': response}, extra={'spider': spider})
            return []


class MedmemeSypderRetryMiddleware(object):
    EXCEPTIONS_TO_RETRY = (defer.TimeoutError, TimeoutError, DNSLookupError,
                           ConnectionRefusedError, ConnectionDone, ConnectError,
                           ConnectionLost, TCPTimedOutError, ResponseFailed,
                           IOError, TunnelError)

    def __init__(self, settings):
        if not settings.getbool('RETRY_ENABLED'):
            raise NotConfigured
        self.max_retry_times = settings.getint('RETRY_TIMES')
        self.retry_http_codes = set(int(x) for x in settings.getlist('RETRY_HTTP_CODES'))
        self.priority_adjust = settings.getint('RETRY_PRIORITY_ADJUST')

    def process_response(self, request, response, spider):
        if request.meta.get('dont_retry', False):
            return response
        if response.status in self.retry_http_codes:
            reason = response_status_message(response.status)
            return self._retry(request, reason, spider) or response
        return response

    def process_exception(self, request, exception, spider):
        if isinstance(exception, self.EXCEPTIONS_TO_RETRY) \
                and not request.meta.get('dont_retry', False):
            return self._retry(request, exception, spider)

    def _retry(self, request, reason, spider, response):
        retries = request.meta.get('retry_times', 0) + 1

        retry_times = self.max_retry_times

        if 'max_retry_times' in request.meta:
            retry_times = request.meta['max_retry_times']

        stats = spider.crawler.stats
        if retries <= retry_times:
            logger.debug("Retrying %(request)s (failed %(retries)d times): %(reason)s",
                         {'request': request, 'retries': retries, 'reason': reason},
                         extra={'spider': spider})
            retryreq = request.copy()
            retryreq.meta['retry_times'] = retries
            retryreq.dont_filter = True
            retryreq.priority = request.priority + self.priority_adjust

            if isinstance(reason, Exception):
                reason = global_object_name(reason.__class__)

            stats.inc_value('retry/count')
            stats.inc_value('retry/reason_count/%s' % reason)
            return retryreq
        else:
            stats.inc_value('retry/max_reached')
            spider.crawler.stats.inc_value('invalid_robotsorsitemap_url/%s' % response.url)
            logger.debug("Gave up retrying %(request)s (failed %(retries)d times): %(reason)s",
                         {'request': request, 'retries': retries, 'reason': reason},
                         extra={'spider': spider})